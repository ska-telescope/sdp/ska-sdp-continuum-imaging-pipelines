#!/usr/bin/env nextflow

datafile = Channel.fromPath('../../yandasoft/data/functest.ms')
imager_config = Channel.fromPath('../config/rascil-imager.in')
checker_config = Channel.fromPath('../config/ci-checker.in')

process imager {
  input:
    path 'functest.ms' from datafile
    path 'rascil-imager.in' from imager_config
  output:
    path 'functest_nmoment1_cip_restored_centre.fits' into restored_image, test_image
    path 'functest_nmoment1_cip_residual.fits' into residual_image
  script:
    """
    python $RASCIL/rascil/apps/rascil_imager.py @rascil-imager.in
    """
}

process checker {
  publishDir 'products', mode: 'copy', overwrite: true
  errorStrategy 'finish'
  input:
    path 'functest_nmoment1_cip_restored_centre.fits' from restored_image
    path 'functest_nmoment1_cip_residual.fits' from residual_image
    path 'ci-checker.in' from checker_config
  output:
    path 'functest*'
    path 'index*'
  script:
    """
    python $RASCIL/rascil/apps/ci_checker_main.py @ci-checker.in
    """
}

process test {
  input:
    path 'functest_nmoment1_cip_restored_centre.fits' from test_image
  script:
    """
    python $RASCIL/rascil/apps/rascil_image_check.py --image functest_nmoment1_cip_restored_centre.fits --stat max --min 0.495 --max 0.505 | awk '{exit(\$1)}'
    """
}
