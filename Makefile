docs:
	sphinx-build docs/ docs/_build

clean:
	-rm -rf docs/_build

.PHONY: docs
