.. _yandasoft:

.. toctree::

*********
YANDASoft
*********

The YANDASoft pipelines are presented in a number of ways.
The simplest is a NextFlow pipeline. NextFlow is a simple computational pipeline described here `<https://www.nextflow.io>`


An individual pipeline is presented for each test. And a common configuration is presented for all tests.

The Functional Test
###################

This test is a simple imaging test it contains a 1Jy point source slightly off the image phase centre. The simulation was generated with the ASKAP simulation tools and is just included here as a demonstration of a simple pipeline.

The Input Data
**************

The measurement set for it is included in `./data/functest.ms` it is a small, 8 channel ASKAP simulation.

The Configuration File
**********************

This deployment of YANDASoft is controlled by a configuration file. All of the aspects of the processing are controlled by this file. This particular implementation is going to image, deconvolve and restore the input simulation.

The configuration file for this processing is included in `./configs/functest.in`::

    Cimager.dataset                                 = [functest.ms]
    Cimager.imagetype                               = casa
    Cimager.memorybuffers                           = true
    Cimager.barycentre                              = false
    Cimager.nchanpercore                            = 8
    Cimager.combinechannels                         = true
    Cimager.Images.Names                            = [image.cont]
    Cimager.Images.shape                            = [128,128]
    Cimager.Images.cellsize                         = [5arcsec, 5arcsec]
    Cimager.visweights                              = MFS
    Cimager.visweights.MFS.reffreq                  = 1.1e+09
    Cimager.Images.image.cont.frequency             = [1.1e+09,1.1e+09]
    Cimager.Images.image.cont.nterms                = 2
    Cimager.Images.image.cont.nchan                 = 1
    Cimager.nworkergroups                           = 3
    Cimager.sensitivityimage                        = true

    Cimager.gridder                                 = WProject
    Cimager.gridder.WProject.wmax                   = 20000
    Cimager.gridder.WProject.nwplanes               = 51
    Cimager.gridder.WProject.oversample             = 8
    Cimager.gridder.WProject.maxsupport             = 1024
    Cimager.gridder.WProject.cutoff                 = 0.001
    Cimager.gridder.WProject.variablesupport        = true
    Cimager.gridder.WProject.offsetsupport          = true

    Cimager.ncycles                                 = 6
    Cimager.Images.writeAtMajorCycle                = true

    # Use a multiscale Clean solver
    Cimager.solver                                  = Clean
    Cimager.solver.Clean.solutiontype               = MAXCHISQ
    Cimager.solver.Clean.decoupled			        = True
    Cimager.solver.Clean.algorithm                  = BasisfunctionMFS
    Cimager.solver.Clean.scales                     = [0]
    Cimager.solver.Clean.niter                      = 10000
    Cimager.solver.Clean.gain                       = 0.3
    Cimager.solver.Clean.logevery                   = 1000
    Cimager.threshold.minorcycle                    = [40%,2mJy,0.18mJy]
    Cimager.threshold.majorcycle                    = 10mJy

    Cimager.solver.Clean.verbose                    = false

    Cimager.preconditioner.preservecf               = true
    Cimager.preconditioner.Names                    = [Wiener]
    Cimager.preconditioner.Wiener.robustness        = 2.0

    Cimager.restore                                 = true
    Cimager.restore.beam                            = fit

I will not go through all the configuration options here they are described at `<https://yandasoft.readthedocs.io/en/latest/calim/imager.html>`.

The important things to note are::

    Cimager.visweights                              = MFS

Which denotes that we are processing using Multi Frequency Synthesis. Which in this case is a representation of the bhaviour of the flux as a fuctional of frequency that is characterised by a Taylor decomposition::

    Cimager.Images.image.cont.nterms                = 2

Our Taylor decomposition will output 2 Taylor-frequency terms. It should be noted that my input simulation has no frequency dependence - so the terms higher than 0 will be empty.

The Taylor term decomposition requires that the same input data be reweighted for each term. This is massively parallel by frequency until deconvolution so is performed in parallel by::

    Cimager.nworkergroups                           = 3

3 Worker groups. All the gridding and imaging will be distributed into 3 groups. Which means the jobs is now configured to run as a 4 (four) rank MPI job.

The gridding is pure `W-projection` as the image has only a very simple source close to the phase centre the imaging is very robust to this. But for completeness the parameters are included.

Other points to note are the `solver` is a basis function solver - but only a 0-basis is being used. A `Wiener filter` is being used instead of tradional weighting.

Running the Test in CI
**********************

This test has been packaged into NextFlow so that the CI will run relatively simply. The relevant section of the `gitlab-ci.yml` is

.. code-block:: yaml
    :linenos:

    test-yandasoft:
        image: registry.gitlab.com/askapsdp/all_yandasoft
        stage: test
        script:
            - echo "The YANDASoft test"
            - apt-get update -y
            - apt-get install -y ca-certificates curl --no-install-recommends
            - apt-get install -y default-jdk
            - cd pipelines/yandasoft/nextflow
            - curl -s https://get.nextflow.io | bash
            - ./nextflow run ./functest.nf
            - mv products/* ${CI_PROJECT_DIR}/products/yandasoft/
        artifacts:
            paths:
                - ${CI_PROJECT_DIR}/products/yandasoft

.. note::
    #. Just the name of the test
    #. The image containing the yandasoft code. At the moment this is a very fat image. Some technical debt has been incurred creating this and a thinner one is under developement
    #. The gitlab CI stage
    #. Script block
    #. Update apt
    #. Install curl
    #. Install java which is a requirement for nextflow
    #. Install nextflow
    #. Run the pipeline
    #. Copy the pipeline products into the output directory
    #. Artifacts declaration - all artifacts once declared are available to subsequent gitlab stages
    #. Expose the following paths as artifacts. These will then be published by the gitlab CI.



The NextFlow Pipeline for the Test
**********************************

NextFlow is a relatively simple mechanism to deploy workflows on multiple
platforms. This is the nextflow script that runs the pipeline.

.. code-block::
    :linenos:

    #!/usr/bin/env nextflow

    config = Channel.fromPath('../configs/functest.in')
    datafile = Channel.fromPath('../data/functest.ms')

    process image {
    input:
        path 'functest.in' from config
        path 'functest.ms' from datafile
    output:
        file 'image.cont.taylor.0.restored' into restored_image, testing_image
        file 'residual.cont.taylor.0' into residual_image
    script:
        """
        mpirun -np 4 --allow-run-as-root imager -c ./functest.in
        """
    }

    process examine {
    input:
        file image from testing_image
    output:
        file 'results.txt' into results
    script:
        """
        imgstat $image > results.txt
        """
    }

    process publish {

    publishDir 'products',mode: 'copy', overwrite: true
    input:
        file image from restored_image
        file resid from residual_image
    output:
        file 'functest.tar.gz'
    script:
        """
        tar -cvf functest.tar $image $resid
        gzip functest.tar
        """
    }

    process test {
    input:
        path 'results.txt' from results

    shell:
        """
        cat results.txt | head -1 | awk '{ if ( \$1 < 0.99 || \$1 > 1.01 ) exit(1); else exit(0) }'
        """
    }


Although this is not meant as a nextflow tutorial the above pretty much covers
all you would need to know. An important concept to grasp is that each process
is completely independent and can be deployed separately - but the direction of
the flow is controlled by the ``channels``. You can see that above the ``test``
process needs the ``results`` channel which is generated by the ``examine`` process,
which itself needs the ``test_image`` channel from the ``image`` process.
Nextflow will deploy these processes as directed by its configuration. In this
case the configuration defaults to a local executor which is the gitlab-ci
runner. The runner is using the image as declared in the CI hence all the
scripts are ran inside that container.

The ``image`` process
^^^^^^^^^^^^^^^^^^^^^

.. code-block::
    :linenos:

    process image {
    input:
        path 'functest.in' from config
        path 'functest.ms' from datafile
    output:
        file 'image.cont.taylor.0.restored' into restored_image, testing_image
        file 'residual.cont.taylor.0' into residual_image
    script:
        """
        mpirun -np 4 --allow-run-as-root imager -c ./functest.in
        """
    }


.. note::

    2. The input declarations - this block defines the input channels.

    3. Bring the config file into this space.

    4. Bring the datafile into this space.

    5. The output channels need to be declared.

    6. Usually there is a one to one mapping. If you need an output to be
    supplied to more than one consumer you have to say so.

    8. All processes need a script block.

    10. Actually run the job as an mpi job over 4 cores (3 for the groups and 1 for the deconvolution).
    The --allow-run-as-root is required as I have not created a default user for this container.

The ``examine`` process
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::
    :linenos:

    process examine {
    input:
        file image from testing_image
    output:
        file 'results.txt' into results
    script:
        """
        imgstat $image > results.txt
        """
    }


.. note::

    3. Get the image from the ``testing_image`` channel of the ``image`` process
    5. Put the results into a results channel.
    6. Run the imgstat tool from YANDASoft to generate some image statistics including the position of the brightest point in the map

The ``test`` process
^^^^^^^^^^^^^^^^^^^^

This parses the results file to see if the brightest point really is a 1 Jy source. This is a simple pass/fail metric for this pipeline. It is not
paticularly clever - but will abort the pipeline and thus the CI job if the brightest point is not 1Jy.

.. code-block::
    :linenos:

    process test {
    input:
        path 'results.txt' from results
    shell:
        """
        cat results.txt | head -1 | awk '{ if ( \$1 < 0.99 || \$1 > 1.01 ) exit(1); else exit(0) }'
        """
    }

.. note::

    4. Note I use ``shell`` instead of ``script`` I found I havd to do this to escape the ``awk`` fields correctly

The ``publish`` process
^^^^^^^^^^^^^^^^^^^^^^^

Ok I need to get the products out of the NextFlow environment and into the gitlab-ci environment for publishing and this is done by my ``publish`` process.

.. code-block::
    :linenos:

    process publish {
    publishDir 'products',mode: 'copy', overwrite: true
    input:
        file image from restored_image
        file resid from residual_image
    output:
        file 'functest.tar.gz'
    script:
        """
        tar -cvf functest.tar $image $resid
        gzip functest.tar
        """
    }

.. note::

   2. This important concept here is that the publishDir is a path generated relative to where nextflow was run. THe ``mode`` is copy as by default it generates links
      ``overwrite`` is set to true. But in the CI this is likeloy only to be run once per build anyway.

   If you look back at the `Running the Test in CI`_ you will see that we move the products into an artifacts directory of the CI run.


The output image
****************

By the way the output of this pipeline is a not-to-interesting point source:

.. image:: images/functest.png
    :width: 200px
    :align: center
    :height: 100px
    :alt: Output of the functional test pipeline


Running the Test Locally
************************

You can probably see from the CI that running locally should be easy. Afterall you could just follow the same steps. But that would require all the pulling of images and the mounting of directories .... surely NextFlow can make that easier. Yes it can. You can set nextflow to use a docker executor locally.

The nextflow config looks like this - it has multiple sections - this is section I use for my local runs

.. code-block::
    :linenos:

    if ( "$HOSTNAME".startsWith("miranda") ) {
        process {
            container = 'registry.gitlab.com/askapsdp/all_yandasoft'
        }
        docker {
            enabled = true
        }
    }

It's pretty much that simple. If I am on my local machine, then use the same
container as in the CI and switch on the docker executor. I need the
conditional as I have other deployment locations.

I have included a ``config.local`` in the repo. So the following should work on any machine with docker and nextflow installed

.. code-block:: bash

    > git clone https://gitlab.com/ska-telescope/sdp/ska-sdp-continuum-imaging-pipelines.git
    > cd ska-sdp-continuum-imaging-pipelines/pipelines/yandasoft/nextflow
    > nextflow -C local.cfg run functest.nf

Running the RASCIL Checker
##########################

There is another nextflow pipeline that runs the funtional test. ``yanda-rascil.nf`` the difference being that the
rascil image checker is ran and the test is performed by that image.

The RASCIL Checker process
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

  process checker {
    container 'artefact.skao.int/rascil-full:0.3.0'
    publishDir 'products', mode: 'copy', overwrite: true
    errorStrategy 'finish'
    input:
       path 'image.cont.taylor.0.restored.fits' from restored_image
       path 'residual.cont.taylor.0.fits' from residual_image
       path 'yanda-rascil-checker.in' from checker_config
    output:
       path 'image*'
       path 'residual*'
       path 'index*'
    script:
       """
       python /rascil/rascil/apps/ci_checker_main.py @yanda-rascil-checker.in
       """
   }

Docker-in-docker invocation
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The important difference is that we are now specifying a different image for this process. This pipeline requires docker-in-docker to run. And the test in the CI file is set up slightly differently If a test stage wishes to invoke docker - then the image for the stage needs to deploy with a running docker daemon.

.. code-block:: bash

  test-yandasoft-w-rascil-w-docker:
  when: manual
  image: docker:19.03.12
  services:
    - docker:19.03.12-dind
  stage: test
  script:
    - echo "The YANDASoft test- with a RASCIL checker - in docker"
    - apk update
    - apk add ca-certificates curl bash openjdk8
    - cd pipelines/yandasoft/nextflow
    - curl -s https://get.nextflow.io | bash
    - ./nextflow run ./yanda-rascil.nf
    - mv products/* ${CI_PROJECT_DIR}/products/yandasoft/
  artifacts:
    paths:
      - ${CI_PROJECT_DIR}/products/yandasoft

You can see it is almost the same - the docker image uses a different package manager. One issue is that the image cache is temporary  -so the full image must
be pulled each time the test is run. This means the test can take a long time to run. Hence this is a manual test.

      
The TST-001 simulation
######################

We have also added a cut-down version of the TST-001 simulation as detailed in the continuum imaging
pipeline confluence pages `<https://confluence.skatelescope.org/x/voKZBw>`_

We have made this an optional test that can be deployed from the gitlab pipeline page (e.g) `<https://gitlab.com/ska-telescope/sdp/ska-sdp-continuum-imaging-pipelines/-/pipelines>`_


The Input Data
**************
The input data here is the gaussian_beams simple simulation and is large (10GB) and it pulled from the
google storage using the following steps in the gitlab_ci.

.. code-block::
    :linenos:

     - echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - && apt-get update -y && apt-get install google-cloud-sdk -y
     - cd ${CI_PROJECT_DIR}/pipelines/yandasoft/data
     - gsutil -m cp -r  "gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331/gaussian_beams.ms" .


The NextFlow Pipeline for TST-001
*********************************

This is identical to the functional test example all we are changing is the input filenames and the Configuration

The Imaging Configuration
*************************

The major difference between this imaging task and the functional test is the size of the input dataset.
There are 100 channels and a large SKA-LOW layout. In order to process this on a system the size of a gitlab_ci
made a number of simplifications to the Imaging.

We have restricted the longest baseline to 10km::

    Cimager.MaxUV					            = 10000

We have likewise restricted the largest w-term to 3km::

    Cimager.gridder.WProject.wmax			= 3000

We have restricted the FOV to 4.5 degrees by reducing the resolution to 10 arc-seconds and
the number of pixels to 1536::

    Cimager.Images.shape	            = [1536,1536]
    Cimager.Images.cellsize	          = [10.27arcsec, 10.27arcsec]

These are not ideal imaging parameters and have only been used to allow the imaging to fit on
a typical runner.


Running the TST-001 Test Locally
********************************

This is exactly the same as the functional test - though it will take much longer

.. code-block:: bash

    > git clone https://gitlab.com/ska-telescope/sdp/ska-sdp-continuum-imaging-pipelines.git
    > cd ska-sdp-continuum-imaging-pipelines/pipelines/yandasoft/nextflow
    > nextflow -C local.cfg run tst-001-low.nf
