#!/usr/bin/env nextflow

config = Channel.fromPath('../configs/yanda-rascil.in')
datafile = Channel.fromPath('../data/functest.ms')
checker_config = Channel.fromPath('../configs/yanda-rascil-checker.in')

process image {
  container 'csirocass/yandasoft:1.1-mpich'
  input:
     path 'yanda-rascil.in' from config
     path 'functest.ms' from datafile
  output:
    file 'image.cont.taylor.0.restored.fits' into restored_image, test_image
    file 'residual.cont.taylor.0.fits' into residual_image 
  script:
    """
    mpirun -np 4 imager -c ./yanda-rascil.in
    """
}

process checker {
  container 'artefact.skao.int/rascil-full:0.3.0'
  publishDir 'products', mode: 'copy', overwrite: true
  errorStrategy 'finish'
  input:
    path 'image.cont.taylor.0.restored.fits' from restored_image
    path 'residual.cont.taylor.0.fits' from residual_image
    path 'yanda-rascil-checker.in' from checker_config
  output:
    path 'image*'
    path 'residual*'
    path 'index*'
  script:
    """
    python /rascil/rascil/apps/ci_checker_main.py @yanda-rascil-checker.in
    """
}


process test {
  container 'artefact.skao.int/rascil-full:0.3.0'
  input:
    path 'image.cont.taylor.0.restored.fits' from test_image
  script:
    """
    python /rascil/rascil/apps/rascil_image_check.py --image  image.cont.taylor.0.restored.fits --stat max --min 0.95 --max 1.05 | awk '{exit(\$1)}'
    """
}
