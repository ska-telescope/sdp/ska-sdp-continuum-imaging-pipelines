#!/usr/bin/env nextflow

datafile = Channel.fromPath('../../yandasoft/data/functest.ms')
imager_config = Channel.fromPath('../config/rascil-imager.in')
qa_config = Channel.fromPath('../config/imaging_qa.in')

process imager {
  input:
    path 'functest.ms' from datafile
    path 'rascil-imager.in' from imager_config
  output:
    path 'functest_nmoment1_cip_restored_centre.fits' into restored_image, test_image
    path 'functest_nmoment1_cip_residual.fits' into residual_image
  script:
    """
    python -m rascil.apps.rascil_imager @rascil-imager.in
    """
}

process qa {
  publishDir 'products', mode: 'copy', overwrite: true
  errorStrategy 'finish'
  input:
    path 'functest_nmoment1_cip_restored_centre.fits' from restored_image
    path 'functest_nmoment1_cip_residual.fits' from residual_image
    path 'imaging_qa.in' from qa_config
  output:
    path 'functest*'
    path 'index*'
  script:
    """
    python -m rascil.apps.imaging_qa_main @imaging_qa.in
    """
}

process test {
  input:
    path 'functest_nmoment1_cip_restored_centre.fits' from test_image
  script:
    """
    python -m rascil.apps.rascil_image_check --image functest_nmoment1_cip_restored_centre.fits --stat max --min 0.495 --max 0.505 | awk '{exit(\$1)}'
    """
}
