#!/usr/bin/env nextflow

config = Channel.fromPath('../configs/gaussian_beams.in')
datafile = Channel.fromPath('../data/gaussian_beams.ms')

process image {
  input:
     path 'gaussian_beams.in' from config
     path 'gaussian_beams.ms' from datafile
  output:
    file 'image.tst-001.taylor.0.restored' into restored_image, testing_image
    file 'residual.tst-001.taylor.0' into residual_image 
  script:
    """
    mpirun -np 4 imager -c ./gaussian_beams.in
    """
}
process examine {
  input:
    file image from testing_image
  output:
     file 'results.txt' into results
  script:
    """
    imgstat $image > results.txt
    """
}
process publish {

  publishDir 'products',mode: 'copy', overwrite: true
  input:
    file image from restored_image
    file resid from residual_image
    file res from results
  output:
    file 'tst-001-low.tar.gz'
  script:
    """
    tar --dereference -cvf tst-001-low.tar $image $resid $res
    gzip tst-001-low.tar
    """
}
  


