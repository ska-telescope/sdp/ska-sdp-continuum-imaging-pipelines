# SDP Continuum Imaging Pipelines

Repository created for SDP continuum imaging pipeline scripts developed in PI9
for [SP-1331](https://jira.skatelescope.org/browse/SP-1331).

This repository is intended to serve two purposes. Firstly as a demonstration of how the pipelines 
can be successfully run. And a respository of pipelines as applied to the continuum imaging testcases as 
developed by SDP.

The way this works is that the pipelines are being added as tasks in the gitlab continuum integration. 
How they are added is essentially up to the groups. It is intended that all pipelines be documented.

It is intended that as more complex testcases and pipelines are developed they will be added to the CI. 
There is a limitation in that the pipelines and tests need to be scoped to a size that can run on the machines
dedicated as runners. However it is also possible to add pipelines and testcases to this repository that are not 
ran as part of CI. 


## Quickstart

### Build sphinx documentation locally

```shell
pip install -r doc-requirements.txt
make docs
```

This will generate sphinx html documentation in the `docs/_build` folder.
