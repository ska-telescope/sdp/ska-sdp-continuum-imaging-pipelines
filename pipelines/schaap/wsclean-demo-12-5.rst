WSClean System Demo 12.5 commands
=================================

This file presents an overview of the ``wsclean`` commands that were used in order to prepare
demo 12.5 (see `<https://confluence.skatelescope.org/display/SE/2021-11-17+DP+ART+System+Demo+12.5>`_).

Please note that the commands provided below require downloading the appropriate measurement sets
from Google Cloud Storage `<gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331/>`_. Also,
for the facet-based runs, appropriate facet files are needed.

The facet files used in the commands presented below (`nfacets_36.reg` and `facets_20k.reg`) can be found in
the same directory as this documentation.
A Python script for generating the facet files can be found in
the `WSClean repository <https://gitlab.com/aroffringa/wsclean/-/blob/master/scripts/ds9_facet_generator.py>`_


Key result 2: scaling issue
---------------------------

(Default) preapplied normalisation mode, yielding incorrect flux scales:
::

    wsclean -log-time -reorder \
        -size 10000 10000 -scale 6asec \
        -beam-mode full \
        -weight briggs 0.0 -padding 1.2 \
        -name screen-based-full-normalisation/wsclean \
        -use-idg -idg-mode hybrid \
        -grid-with-beam -aterm-kernel-size 10 \
        -beam-aterm-update 1200 -channel-range 0 10 \
        realistic_dipole_element_beams.ms


Amplitude normalisation:

::

    wsclean -log-time -reorder \
        -size 10000 10000 -scale 6asec \
        -beam-mode full -beam-normalisation-mode amplitude\
        -weight briggs 0.0 -padding 1.2 \
        -name screen-based-full-normalisation/wsclean \
        -use-idg -idg-mode hybrid \
        -grid-with-beam -aterm-kernel-size 10 \
        -beam-aterm-update 1200 -channel-range 0 10 \
        realistic_dipole_element_beams.ms


Key result 4: computing times facet-based imaging (with beam)
-------------------------------------------------------------

For facet-based imaging, ``NTHREADS=[2, 4, 8, 16, 32]``


Facet-based imaging, with beam:
*******************************
::

    wsclean -log-time -reorder -j 1 -parallel-gridding [NTHREADS]  \
        -size 10000 10000 -scale 6asec -mgain 0.8 \
        -niter 1000000 -nmiter 30 -auto-threshold 3.0 -auto-mask 5.0 \
        -weight briggs 0.0 -padding 1.2 -name facet-based-parallelgrid[[NTHREADS]]-beam/wsclean -use-wgridder \
        -facet-regions nfacets_36.reg -apply-facet-beam -beam-mode array_factor -facet-beam-update 1200 \
        -channel-range 0 10 \
        realistic_isotropic_element_beams.ms

Facet-based imaging, without beam:
**********************************

::

    wsclean -log-time -reorder -j 1 -parallel-gridding [NTHREADS]  \
        -size 10000 10000 -scale 6asec -mgain 0.8 \
        -niter 1000000 -nmiter 30 -auto-threshold 3.0 -auto-mask 5.0 \
        -weight briggs 0.0 -padding 1.2 -name facet-based-parallelgrid[NTHREADS]-beam/wsclean -use-wgridder \
        -facet-regions nfacets_36.reg \
        -channel-range 0 10 \
        realistic_isotropic_element_beams.ms


Screen-based imaging, with beam
*******************************
For a cpu-only run, replace ``-idg-mode hybrid`` by ``-idg-mode cpu``.

::

    wsclean -log-time -reorder -size 10000 10000 -scale 6asec \
        -mgain 0.8 -niter 1000000 -nmiter 15 -beam-mode array_factor \
        -auto-threshold 3.0 -auto-mask 5.0 -weight briggs 0.0 -padding 1.2 \
        -name screen-based-hybrid-array-factor/wsclean \
        -use-idg -idg-mode hybrid -grid-with-beam -aterm-kernel-size 10 -beam-aterm-update 1200 \
        -channel-range 0 10 \
        realistic_isotropic_element_beams.ms

Screen-based imaging, without beam
**********************************
For a cpu-only run, replace ``-idg-mode hybrid`` by ``-idg-mode cpu``.

::

    wsclean -log-time -reorder -size 10000 10000 -scale 6asec \
        -mgain 0.8 -niter 1000000 -nmiter 15 -beam-mode array_factor \
        -auto-threshold 3.0 -auto-mask 5.0 -weight briggs 0.0 -padding 1.2 \
        -name screen-based-hybrid-array-factor/wsclean \
        -use-idg -idg-mode hybrid \
        -channel-range 0 10 \
        realistic_isotropic_element_beams.ms

Key result 4: scientific quality
--------------------------------

Facet-based imaging
*******************

::

    wsclean -log-time -reorder -parallel-gridding 32 -size 20000 20000 -scale 3asec -mgain 0.6 \
        -niter 1000000 -nmiter 25 -beam-mode full -auto-threshold 3.0 -auto-mask 5.0 \
        -weight briggs 0.0 -padding 1.2 \
        -name facet-based-full-beam-invms-20chan/wsclean \
        -use-wgridder -facet-regions facets_20k.reg -apply-facet-beam -facet-beam-update 1200 \
        -channel-range 0 20 -channels-out 2 -join-channels realistic_dipole_element_beams_invbeam.ms

where ``realistic_dipole_element_beams_invbeam.ms`` is created from ``realistic_dipole_element_beams.ms`` using ``DP3``, see
`<https://jira.skatelescope.org/browse/AST-568?focusedCommentId=98763&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-98763>`_.

Screen-based imaging
********************

::

    wsclean -log-time -reorder -size 20000 20000 -scale 3asec -mgain 0.7 \
        -niter 1000000 -nmiter 20 -beam-mode full -beam-normalisation-mode amplitude \
        -auto-threshold 3.0 -auto-mask 5.0 -weight briggs 0.0 -padding 1.2 \
        -name screen-based-hybrid-full-beam-20chan/wsclean \
        -use-idg -idg-mode hybrid -grid-with-beam -aterm-kernel-size 10 -beam-aterm-update 1200 \
        -channel-range 0 20 -channels-out 2 -join-channels \
        realistic_dipole_element_beams.ms
