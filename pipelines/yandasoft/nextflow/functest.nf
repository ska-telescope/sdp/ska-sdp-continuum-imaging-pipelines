#!/usr/bin/env nextflow

config = Channel.fromPath('../configs/functest.in')
datafile = Channel.fromPath('../data/functest.ms')

process image {
  input:
     path 'functest.in' from config
     path 'functest.ms' from datafile
  output:
    file 'image.cont.taylor.0.restored' into restored_image, testing_image
    file 'residual.cont.taylor.0' into residual_image 
  script:
    """
    mpirun -np 4 imager -c ./functest.in
    """
}
process examine {
  input:
    file image from testing_image
  output:
     file 'results.txt' into results
  script:
    """
    imgstat $image > results.txt
    """
}
process publish {

  publishDir 'products',mode: 'copy', overwrite: true
  input:
    file image from restored_image
    file resid from residual_image
  output:
    file 'functest.tar.gz'
  script:
    """
    tar --dereference -cvf functest.tar $image $resid
    gzip functest.tar
    """
}
process test {
  input:
    path 'results.txt' from results

  shell:
    """
    cat results.txt | head -1 | awk '{ if ( \$1 < 0.99 || \$1 > 1.01 ) exit(1); else exit(0) }'
    """
} 
  


