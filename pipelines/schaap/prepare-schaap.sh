#!/bin/bash

if [ -z "$DATA_DIR" ]
then
  export DATA_DIR=data
fi

mkdir -p "${DATA_DIR}/realistic_isotropic_element_beams.ms"
gsutil -m rsync -r "gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331/realistic_isotropic_element_beams.ms" "${DATA_DIR}/realistic_isotropic_element_beams.ms"
