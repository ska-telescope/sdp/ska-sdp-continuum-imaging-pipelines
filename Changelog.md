# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
Added some pip install infrastucture (doc-requirements) and slightly changed the README
Also added the first test a Yandasoft point source imaging test managed by NexfFlow in the CI
