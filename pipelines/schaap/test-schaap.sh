#!/bin/bash

if [ -z "$DATA_DIR" ]
then
  export DATA_DIR=data
fi

echo $DATA_DIR

#
# case 1. screen based
#

wsclean -interval 0 10 -nmiter 1 -reorder -size 10000 10000 -scale 6asec \
-mgain 0.8 -niter 1000000 \
-auto-threshold 3.0 -auto-mask 5.0 -weight briggs 0.0 -padding 1.2 \
-name screen-based-cpu_mpi01_parallelgrid1_nobeam \
-use-idg -idg-mode cpu -channel-range 0 10 \
"${DATA_DIR}/realistic_isotropic_element_beams.ms"

#
# case 2. facet based
#

wsclean -interval 0 5  -nmiter 1 -reorder -size 10000 10000 -scale 6asec -mgain 0.8 -niter 1000000 \
-auto-threshold 3.0 -auto-mask 5.0 -weight briggs 0.0 -padding 1.2 \
-name facet-based_mpi01_parallelgrid20_nobeam -use-wgridder \
-facet-regions nfacets_36.reg -channel-range 0 10 \
"${DATA_DIR}/realistic_isotropic_element_beams.ms"

#
# case 3. screen based with beam corrections 
# (scaling of output imaage seems wrong)
#
wsclean -reorder -size 10000 10000 -scale 6asec -mgain 0.8 -niter 1000000 \
-auto-threshold 3.0 -auto-mask 5.0 -weight briggs 0.0 -padding 1.2 \
-name screen-based-hybrid_mpi01_parallelgrid1_beam -use-idg -idg-mode cpu \
-grid-with-beam -aterm-kernel-size 10 -beam-aterm-update 1200 -channel-range 0 10 \
"${DATA_DIR}/realistic_isotropic_element_beams.ms"


#
# case 4. facet based with beam corrections 
# (deconvolution diverges)
#

wsclean -reorder -size 10000 10000 -scale 6asec -mgain 0.8 -niter 1000000 \
-auto-threshold 3.0 -auto-mask 5.0 -weight briggs 0.0 -padding 1.2 \
-name facet-based_mpi01_parallelgrid20_beam -use-wgridder -facet-regions nfacets_36.reg \
-apply-facet-beam -pb-undersampling 50 -facet-beam-update 1200 -channel-range 0 10 \
"${DATA_DIR}/realistic_isotropic_element_beams.ms"
