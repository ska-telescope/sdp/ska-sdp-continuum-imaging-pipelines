#!/bin/bash

# To be run from pipelines/rascil

python3 -m rascil.apps.imaging_qa_main \
  --ingest_fitsname_restored data/functest_nmoment2_cip_restored_centre.fits \
  --ingest_fitsname_residual data/functest_nmoment2_cip_residual.fits \
  --finder_beam_maj 0.007 --finder_beam_min 0.0055 --finder_beam_pos_angle 3.0
