Introduction
============

The scripts in this directory, together with the test-schaap job defined in the .gitlab-ci.yml file in the root of this repository, can be used to run WSClean on SKA LOW simulated data.

These scripts were written in the context of `<https://jira.skatelescope.org/browse/AST-485>`_
implementing `<https://jira.skatelescope.org/browse/SP-1693>`_.

The imaging scenarios are given at `<https://confluence.skatelescope.org/display/SE/Continuum+imaging+standard+outputs>`_.


Setup
=====

The setup is such that the scripts can be run on

1. A single node with gitlab-runner and docker or singularity installed,
2. A cluster that has gitlab-runner installed on the head node and
   singularity on the compute nodes,
3. CI environment of this repository, using gitlab-runner + docker.
   Note that the default runners do not have sufficient resources
   to complete the job within the 60 minute time limit.


Gitlab CI artifacts/caching
===========================

Artifacts are used to send data from one job to another, and
caching to share data between jobs, possibly over multiple runs.
For both methods the shared files are archived at the end of the job and extracted at the beginning
of the next job. 
In the initial setup the download of the measurement set and imaging
were split over differente stages/jobs.
It turned out that for large datasets, like a measurement set,
extracting the archive takes almost as much time as downloading data.
Therefore downloading and imaging of the data is currently done in a single job.

When gitlab-runner is run in a shared environment (shell or ssh)
the environment variable `DATA_DIR` can be specified. 
If the measurement set is already present there, it will not be downloaded again.


Running manually
================

To start the gitlab CI job ``test-schaap`` manually use::

  gitlab-runner exec shell test-schaap


With a data directory, to prevent unnecessary downloads of the measurement set::

  gitlab-runner exec shell --env DATA_DIR=<data_dir> test-schaap

To launch it on a compute node over ssh::
  gitlab-runner exec ssh --ssh-host <compute_node_name> --ssh-user $USER --ssh-identity-file $HOME/.ssh/id_dsa test-schaap

Or, when singularity is installed, but gitlab-runner not::
::
  singularity run docker://gitlab/gitlab-runner exec ssh --builds-dir /var/scratch/bvdtol/builds --ssh-host <compute_node_name> --ssh-user $USER --ssh-identity-file $HOME/.ssh/id_dsa test-schaap

Running in local CI
===================

#. Create a fork from https://gitlab.com/ska-telescope/sdp/ska-sdp-continuum-imaging-pipelines
#. register gitlab-runner
    * Get a registration token from the forked repository at gitlab.com. The token can be found in the menu Settings --> CI/CD --> Runners
    * ``gitlab-runner register``
#. Start gitlab-runner with ``gitlab-runner run``
#. Trigger a pipeline run in the CI/CD menu of the webinterface of the forked repository.


Running gitlab-runner from docker/singularity
=============================================

In case gitlab-runner is not installed it can be run from the docker image `gitlab/gitlab-runner`, using docker::

  docker run gitlab/gitlab-runner

or singularity::

  singularity run docker://gitlab/gitlab-runner

Launch WSClean manually from the wsclean:3.0 image
==================================================

Using docker::

    docker run docker.io/astronrd/wsclean:3.0 wsclean --version

Using singularity::

    singularity run docker://docker.io/astronrd/wsclean:3.0 wsclean --version


