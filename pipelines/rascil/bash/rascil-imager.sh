#!/bin/bash

# To be run from pipelines/rascil

python3 -m rascil.apps.rascil_imager \
  --ingest_msname data/functest.ms \
  --ingest_dd 0 \
  --ingest_vis_nchan 16 \
  --ingest_chan_per_blockvis 1 \
  --ingest_average_blockvis False \
  --imaging_npixel 512 \
  --imaging_cellsize 1e-05 \
  --imaging_weighting robust \
  --imaging_robustness -0.5 \
  --clean_nmajor 6 \
  --clean_algorithm mmclean \
  --clean_scales 0 \
  --clean_fractional_threshold 0.1 \
  --clean_threshold 1e-3 \
  --clean_nmoment 2 \
  --clean_restored_output integrated
